@AddUser
Feature: Add a user to a table


  Scenario Outline: Add a user to a table

    Given I am on the way2Automation webTables
    And  I validate that i am on the user list table
    And I click on Add user
    And I add a user with the following details: "<fistName>", "<lastName>", "<password>", "<customer>", "<role>", "<email>" and "<cell>"
    Then I verify if the user is added


    Examples:

      | fistName | lastName | password | customer    | role     | email             | cell    |
      | FName1   | LName1   | Pass1    | Company AAA | Admin    | admin@mail.com    | 0852555 |
      | FName2   | LName1   | Pass2    | Company BBB | Customer | customer@mail.com | 083444  |











      