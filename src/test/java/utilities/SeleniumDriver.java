package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SeleniumDriver  {

    private static SeleniumDriver seleniumDriver;
    private static WebDriver driver;

    private static WebDriverWait webDriverWait;

    public final static int TIMEOUT =130;
    public final static int PAGE_LOAD_TIMEOUT =150;


    public SeleniumDriver(){
        System.setProperty("webdriver.chrome.driver", "/Users/automation/Downloads/cib front assesment/resources/lib/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        webDriverWait = new WebDriverWait(driver,TIMEOUT);
        driver.manage().timeouts().implicitlyWait(TIMEOUT,TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT,TimeUnit.SECONDS);
    }
    public  static void openPage(String url){
        driver.get(url);
    }
    public static WebDriver getDriver(){
        return driver;
    }
    public static void setupDriver(){

        if (seleniumDriver==null){
            seleniumDriver = new SeleniumDriver();
        }
    }
    public static void tearDown(){

        if (driver!=null){

            driver.close();
            driver.quit();
        }
        seleniumDriver = null;

    }

}
