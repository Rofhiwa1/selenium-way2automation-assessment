package utilities;

public class ExtentReportConfig {

    public String getReportConfigPath() {
        String reportConfigPath ="C:\\Users\\ABRMADW\\iLabAutomationAssesment\\src\\test\\resources\\extent-config.xml";
        if (reportConfigPath != null) return reportConfigPath;
        else throw new RuntimeException("Report Config Path not specified in the Configuration.properties");
    }

}
