package pageObjectRepo.actions;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import pageObjectRepo.elements.way2WebTablesAddUserForm;
import utilities.SeleniumDriver;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

public class way2WebTablesAddUserFormActions {


    way2WebTablesAddUserForm way2WebTablesAddUserFormLocator = null;
    way2WebTablesActions WebTablesAction = null;



    public way2WebTablesAddUserFormActions() {
        this.way2WebTablesAddUserFormLocator = new way2WebTablesAddUserForm();
        PageFactory.initElements(SeleniumDriver.getDriver(), way2WebTablesAddUserFormLocator);
    }


    //action implementation to type name
    public void typeFistName(String fistName) {

        way2WebTablesAddUserFormLocator.firstNameField.sendKeys(fistName);
    }
    //action implementation to type lastName
    public void typeLastName(String lastName) {

        way2WebTablesAddUserFormLocator.lastNameField.sendKeys(lastName);
    }

    //action implementation to type the userName
    public void typeUserName(String userName) {


        way2WebTablesAddUserFormLocator.userNameField.sendKeys(userName);
    }

    //action implementation to type the password
    public void typePassword(String password) {

        way2WebTablesAddUserFormLocator.passwordField.sendKeys(password);
    }

    //select the radio button depending on what the customer type is
    public void selectCustomerType(String customer) {

        if (customer.equalsIgnoreCase("Company AAA")) {
            way2WebTablesAddUserFormLocator.CustomerAAARadioButton.click();
        } else if (customer.equalsIgnoreCase("Company BBB")) {

            way2WebTablesAddUserFormLocator.CustomerBBBRadioButton.click();
        }
    }


    //logic to support the set role
    private Select selectRole() {

        return new Select(way2WebTablesAddUserFormLocator.roleDropDownList);
    }


    // //action implementation to select row from the drop downlist
    public void setRole(String role) {

        selectRole().selectByVisibleText(role);
    }


    //action implementation to type the email address
    public void typeEmailAddress(String typeEmail) {

        way2WebTablesAddUserFormLocator.emailField.sendKeys(typeEmail);
    }

    //action implementation to type the cellphone number
    public void typeCellPhone(String cellPhone) {

        way2WebTablesAddUserFormLocator.cellPhoneField.sendKeys(cellPhone);
    }

    //action implementation to click the save button
    public void clickSave() {

        way2WebTablesAddUserFormLocator.saveButton.click();
    }


    //This is the method used to generate a unique name for the user name as per the spec
    public String generateUniqueUserName() {

        String uniqueUserName = RandomStringUtils.random(5, true, true);
        WebTablesAction = new way2WebTablesActions();
        List<String> availableUserNames = WebTablesAction.readUserNameColumnInfo();

        //checks if the generated string does not match with anything on the table
        if (!availableUserNames.contains(uniqueUserName)) {
            return uniqueUserName;

        } else {
            Assert.fail("could not generate a unique userName");
            return "could not generate a unique userName";

        }


    }


}
















