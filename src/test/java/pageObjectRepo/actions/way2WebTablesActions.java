package pageObjectRepo.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pageObjectRepo.elements.way2WebTablesPage;
import utilities.SeleniumDriver;

import java.util.ArrayList;
import java.util.List;

public class way2WebTablesActions {


    private way2WebTablesPage way2WebTablesPageLocator = null;

    WebElement myTable;
    List<WebElement> rows;
    List<WebElement> columns;
    int columnSize;
    private int rowSize;
    List <String> userNameColumnData;


    //Initializes the page with all the element in the way2WebTablesPage page
    public way2WebTablesActions(){
        this.way2WebTablesPageLocator = new way2WebTablesPage();
        PageFactory.initElements(SeleniumDriver.getDriver(),way2WebTablesPageLocator);
    }


    //Action to click the add user button
    public void clickOnAddUser(){

        way2WebTablesPageLocator.AddUser.click();
    }

    //Action view the page title
    public String verifyTablesPage(){

       return SeleniumDriver.getDriver().getTitle();
    }



    //This is the method that reads information on the user name column, it will be used to verify that the user name does not get duplicated
    //Also used to verify is the user was added in the table
    public List<String> readUserNameColumnInfo(){

        //get the table
        myTable = SeleniumDriver.getDriver().findElement(By.xpath("/html/body/table/tbody"));
        //gets all the rows in the table
        rows = myTable.findElements(By.tagName("tr"));
        //list to store column data
        userNameColumnData = new ArrayList<String>();

        //Row size used to control the loop
        rowSize = rows.size();

        //logic to read data from the username column
        for(int i = 0; i<rowSize; i ++){


            columns = rows.get(i).findElements(By.tagName("td"));

            columnSize = columns.size();
            for (int j = 0; j<columnSize; j++){

                //2 is the index for the column name.
                String cellData = columns.get(2).getText();

                if (j==2){

                    userNameColumnData.add(cellData);
                }
            }
        }
        return userNameColumnData;
    }
}
