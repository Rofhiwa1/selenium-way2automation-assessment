package pageObjectRepo.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class way2WebTablesAddUserForm {


    //Bellow are the elements for the Add user form element locators

    @FindBy(how = How.NAME,using ="FirstName")
    public WebElement firstNameField;

    @FindBy(how = How.NAME,using ="LastName")
    public WebElement lastNameField;

    @FindBy(how = How.NAME,using ="UserName")
    public WebElement userNameField;

    @FindBy(how = How.NAME,using ="Password")
    public WebElement passwordField;

    @FindBy(how = How.XPATH,using ="/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[1]")
    public WebElement CustomerAAARadioButton;

    @FindBy(how = How.XPATH,using ="/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[2]")
    public WebElement CustomerBBBRadioButton;

    @FindBy(how = How.NAME,using ="RoleId")
    public WebElement roleDropDownList;

    @FindBy(how = How.NAME,using ="Email")
    public WebElement emailField;

    @FindBy(how = How.NAME,using ="Mobilephone")
    public WebElement cellPhoneField;

    @FindBy(how = How.XPATH,using ="/html/body/div[3]/div[3]/button[2]")
    public WebElement saveButton;



}
