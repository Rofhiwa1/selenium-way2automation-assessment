package stedefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.testng.Assert;
import pageObjectRepo.actions.*;
import utilities.SeleniumDriver;

public class AddAUserToWebTable {


    private  way2WebTablesAddUserFormActions way2WebTablesAddUserFormAction = new way2WebTablesAddUserFormActions();
    private   way2WebTablesActions way2WebTablesAction = new way2WebTablesActions();
    private String userName;


    // step that initiates opens the web page
    @Given("^I am on the way2Automation webTables$")
    public void i_am_on_the_iLab_Homepage(){
       SeleniumDriver.openPage("http://www.way2automation.com/angularjs-protractor/webtables/");
    }



   // The step that validates the page heading
    @Given("^I validate that i am on the user list table$")
    public void i_validate_that_i_am_on_the_user_list_table() throws Throwable {


        Assert.assertEquals("Protractor practice website - WebTables",way2WebTablesAction.verifyTablesPage());


        System.out.println(way2WebTablesAction.readUserNameColumnInfo());

    }


    //click on add user button step
    @Given("^I click on Add user$")
    public void i_click_on_Add_user() throws Throwable {
        way2WebTablesAction.clickOnAddUser();

    }


    //This is the step we fill out the user details as per the feature file. Note: User name was generated as seen in the way2WebTablesAddUserFormActions class
    @Given("^I add a user with the following details: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_add_a_user_with_the_following_details_and(String fistName, String lastName, String password, String customer, String role, String email, String cellPhone) throws Throwable {

        userName = way2WebTablesAddUserFormAction.generateUniqueUserName();

        way2WebTablesAddUserFormAction.typeFistName(fistName);
        way2WebTablesAddUserFormAction.typeLastName(lastName);
        way2WebTablesAddUserFormAction.typeUserName(userName);
        way2WebTablesAddUserFormAction.typePassword(password);
        way2WebTablesAddUserFormAction.selectCustomerType(customer);
        way2WebTablesAddUserFormAction.setRole(role);
        way2WebTablesAddUserFormAction.typeEmailAddress(email);
        way2WebTablesAddUserFormAction.typeCellPhone(cellPhone);
        way2WebTablesAddUserFormAction.clickSave();

    }


    //This step verifies if the user has been added using the userName as the unique identifier
    @Then("^I verify if the user is added$")
    public void i_verify_if_the_user_is_added() throws Throwable {


        if(way2WebTablesAction.readUserNameColumnInfo().contains(userName)){
            Assert.assertTrue(true);
        }
        else {

            Assert.fail("The user added is not appearing on the list");
        }


    }



}
