package stedefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import utilities.SeleniumDriver;

public class Hooks {

    @Before
    public void setUp(){
        //This is to setup the driver before the test
        SeleniumDriver.setupDriver();
    }

    @After
    public static void tearDown(Scenario scenario) {


         //This closes the browser after a test
        SeleniumDriver.tearDown();
    }

}
